/*global $, alert */
/*jslint browser: true, indent: 2 */

var runApp = function () {
  var xmlData, lines = {}, outages = [], transformXmlData, dataFetchError,
    helpers, url = "http://www.mta.info/developers/data/nyct/nyct_ene.xml";

  helpers = {
    capitalize:  function (string) {
      string = string.toLowerCase();
      return string.charAt(0).toUpperCase() + string.slice(1);
    }
  };

  transformXmlData = function (data) {
    lines = {};
    outages = [];
    var xmlData = data.responseText;
    $(xmlData).find('outage').each(function () {
      var i, trainnos, trains, outageIdx, outage = {};

      $(this).children().each(function () {
        outage[this.tagName.toLowerCase()] = this.textContent;
      });

      trainnos = outage.trainno;
      trains = trainnos.split('/');
      outageIdx = outages.length;

      for (i = trains.length; i--;) {
        var train = trains[i].toLowerCase();
        if (train) { // yeah, can be empty if they leave a trailing'/'.
          if (!lines[train]) {
            lines[train] = [];
          }
          lines[train].push(outageIdx);
        }
      }
      outages.push(outage);
    });
    console.log('Lines', lines);
    console.log('Outages', outages);
  };

  dataFetchError = function (error) {
    // TODO Do something :)
  };

  $.ajax({
    type: "GET",
    url: url,
    dataType: "xml",
    beforeSend: function () {
      $.mobile.showPageLoadingMsg('a', 'Fetching the Latest Data', false);
    },
    success: transformXmlData,
    error: dataFetchError,
    complete: function () {
      window.setTimeout(function () {
        $.mobile.loading("hide");
      }, 350);
    }
  });

  var isFirstRun = true;

  $('.line').on('click', function (e) {
    e.stopPropagation();

    if (!$(this).hasClass('inactive') && !isFirstRun) return;

    if (isFirstRun) {
      $('.intro').slideUp();
    }

    $(this).removeClass('inactive');

    var i,
      line = e.target.id,
      stationIdxs = lines[line] || [],
      outageStations = {},
      $notice = $('#notice-details');

    var $lines = $('.line');

    if (isFirstRun) {
      $lines.fadeTo('slow', 0.3);
    }

    $lines.addClass('inactive');

    if (isFirstRun) {
      $lines.fadeTo('fast', 1.0);
      isFirstRun = false;
    }

    $(this).removeClass('inactive');

    for (i = stationIdxs.length; i--;) {
      var outageStation = outages[stationIdxs[i]];
      if (!outageStations[outageStation.station]) {
        outageStations[outageStation.station] = [];
      }
      outageStations[outageStation.station].push(outageStation);
    }

    $notice.html('');
    $notice.hide();

    for (outageStation in outageStations) {
      var $div = $('<div>');
      $div.append($('<h3>').html(outageStation.toLowerCase()));
      var outagesArr = outageStations[outageStation];

      for (i = outagesArr.length; i--;) {
        var outage = outagesArr[i];
        /*
         * The <machineType> serving <serving> is out of service due to <reason>,
         * it is expected to service
         */
        var machine = (outage.equipmenttype === 'ES') ? 'escalator' : 'elevator';
        var msg = 'The <b>' + machine + '</b> serving <b>' + outage.serving.toLowerCase() + '</b> is out of service due to <b>' + outage.reason.toLowerCase() + '</b> until <b>' + outage.estimatedreturntoservice.toLowerCase() + '</b>.';
        $div.append($('<p>').html(msg));
      }
      $notice.append($div);
      $div.attr('data-role', 'collapsible');
    }

    $notice.collapsibleset('refresh');
    $notice.fadeIn();
  });
};

$(runApp);